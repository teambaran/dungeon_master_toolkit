# Dungeon_master_toolkit
Developed in a team of 4 using Android Studio.
Application allows for multiple devices to connect via the Google Nearby Connections API.
Intended to be used when playing the tabletop Dungeons and Dragons RPG.
Allows for a Dungeon master to see all player's items, gold, and status.
I was tasked with configuring Nearby Connections as well as designing and implementing the UI.
